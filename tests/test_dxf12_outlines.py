# coding=utf-8
from dxf12_outlines import MyEffect
from tests.base import InkscapeExtensionTestMixin, TestCase


class TestDXF12OutlinesBasic(InkscapeExtensionTestMixin, TestCase):
    def setUp(self):
        self.effect = MyEffect
        self.e = self.effect()
